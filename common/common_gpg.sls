#!yaml|gpg

users:
 cloud_user:
   password: |
     -----BEGIN PGP MESSAGE-----
     Version: GnuPG v2.0.22 (GNU/Linux)

     hQEMA86rrGwfdO21AQf/SS8jTuotjgbX4U+RBegTLw+3WkTPX0vaXT4S2S/ILhde
     bbG15MWTM9FVGyE2EcqFoxaireW8lzPW7H+qW1A8vps5saCeZ4uoMmzpbCC5NQBb
     IzxCKjTyl700QBvqiAOhvSG9Vxz5ZRuuo5smxuSKDMFx+wUnXuaC4vOsd3xWiBkd
     l+2zRY8h01ztRH8u5cdrr9+2AK/Vc5vZ1nczF122saIjJ3SA6d39YdjD1mwOIvh2
     Iv5/UhScLLPNYmSgQKBj2BqXLi6bzG9DpbHwB9vSgODDv0irBkIHxp6cMrdrXruC
     WMZGLmeFWpGJkSFU6z2ytkgaT69aZQx7J1ZXO0yGaNJdAVaxa30zX/8axuBnSN8D
     ZV2VHagEq+0Z293pS/EcSnPSY2zh/uaGUiVJmnMpNjt7YorXcXx42NiJA2CAlQIw
     TaGLU0dSCQThLBR0uhaLCygkP4xjBDl4A8ACUcsb
     =p7zt
     -----END PGP MESSAGE-----
 container_user:
   password: |
     -----BEGIN PGP MESSAGE-----
     Version: GnuPG v2.0.22 (GNU/Linux)

     hQEMA86rrGwfdO21AQf/SS8jTuotjgbX4U+RBegTLw+3WkTPX0vaXT4S2S/ILhde
     bbG15MWTM9FVGyE2EcqFoxaireW8lzPW7H+qW1A8vps5saCeZ4uoMmzpbCC5NQBb
     IzxCKjTyl700QBvqiAOhvSG9Vxz5ZRuuo5smxuSKDMFx+wUnXuaC4vOsd3xWiBkd
     l+2zRY8h01ztRH8u5cdrr9+2AK/Vc5vZ1nczF122saIjJ3SA6d39YdjD1mwOIvh2
     Iv5/UhScLLPNYmSgQKBj2BqXLi6bzG9DpbHwB9vSgODDv0irBkIHxp6cMrdrXruC
     WMZGLmeFWpGJkSFU6z2ytkgaT69aZQx7J1ZXO0yGaNJdAVaxa30zX/8axuBnSN8D
     ZV2VHagEq+0Z293pS/EcSnPSY2zh/uaGUiVJmnMpNjt7YorXcXx42NiJA2CAlQIw
     TaGLU0dSCQThLBR0uhaLCygkP4xjBDl4A8ACUcsb
     =p7zt
     -----END PGP MESSAGE-----


